#GIT
FROM alpine/git as repo
WORKDIR /app
RUN git clone https://gitlab.com/vinayomkar/javaapp.git

#MAVEN
FROM maven:3.3.9-alpine as build
WORKDIR /app
COPY --from=repo /app/javaapp /app
RUN mvn install

#TOMCAT
FROM tomcat
RUN sed -i '69s/8080/9090/' /usr/local/tomcat/conf/server.xml
COPY --from=build /app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/Spring3HibernateApp.war

